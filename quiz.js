// const quizContainer = document.getElementById('quiz');
// const resultsContainer = document.getElementById('results');
// const submitButton = document.getElementById('submit');

// function buildQuiz(){

//  //place to store the html output
//    const output = [];

//    //for each question..

//    myQuestions.forEach(
// (currentQuestion, questionNumber) => {

// //storing the list of answer choices
// const answers = [];


// // and for each available answer...
// for(letter in currentQuestion.answers){

// // HTML radio button
// answers.push(
// `<label>
//   <input type="radio" name="question${questionNumber}" value="${letter}">
//   ${letter} :
//   ${currentQuestion.answers[letter]}
// </label>`
// );
// }

// // add this question and its answers to the output
// output.push(
//   `<div class="question"> ${currentQuestion.question} </div>
//   <div class="answers"> ${answers.join('')} </div>`
// );
// });

// // finally combine our output list into one string of HTML and put it on the page
// quizContainer.innerHTML = output.join('');
// }

// function showResults(){
 
  
//   // gather answer containers from our quiz
//   const answerContainers = quizContainer.querySelectorAll('.answers');

//   // keep track of user's answers
//   let numCorrect = 0;

//   // for each question...
//   mainQuestions.forEach( (currentQuestion, questionNumber) => {

//     // find selected answer
//     const answerContainer = answerContainers[questionNumber];
//     const selector = 'input[name=question'+questionNumber+']:checked';
//     const userAnswer = (answerContainer.querySelector(selector) || {}).value;

//     // if answer is correct
//     if(userAnswer===currentQuestion.correctAnswer){
//       // add to the number of correct answers
//       numCorrect++;

//       // color the answers green
//       answerContainers[questionNumber].style.color = 'lightgreen';
//     }
//     // if answer is wrong or blank
//     else{
//       // color the answers red
//       answerContainers[questionNumber].style.color = 'red';
//     }
//   });

//   // show number of correct answers out of total
//   resultsContainer.innerHTML = numCorrect + ' out of ' + mainQuestions.length;
// }

// const answerContainer = document.getElementById("quiz");
// }

// // display quiz right away

// buildQuiz();

// // on submit, show results

// submitButton.addEventListener('click', showResults);

// //array of questions and answers

// const mainQuestions = [
//   {
//     question: "What is the full meaning of VGG?",
//     answers: {
//       a: "Venture Garden Group",
//       b: "Viable Ganja Ground ",
//       c: "none of the above",
//       d: "all of the above"
//     },
//     correctAnswer: "a"
//   },
//   {
//     question: "What year was VGG found?",
//     answers: {
//       a: "2018",
//       b: "1981",
//       c: "2010",
//       d: "2003"
//     },
//     correctAnswer: "c"
//   },
//   {
//     question: "How many SBUs' does VGG have?",
//     answers: {
//       a: "10",
//       b: "6",
//       c: "5",
//       d: "20"
//     },
//     correctAnswer: "c"
//   }, 
//   {
//     question: "Who is the CEO of VGG?",
//     answers: {
//       a: "Tesilimi",
//       b: "Seun",
//       c: "Theophilus",
//       d: "Bunmi"
//     },
//     correctAnswer: "d"
//   },
//   {
//     question: "What is imposible?",
//     answers: {
//       a: "everything",
//       b: "nothing",
//       c: "something",
//       d: "all of the above"
//     },
//     correctAnswer: "b"
//   },
//   {
//     question: "How many core values does VGG have?",
//     answers: {
//       a: "15",
//       b: "7",
//       c: "5",
//       d: "1"
//     },
//     correctAnswer: "7"
//   },
//   {
//     question: "One of these is a core value..",
//     answers: {
//       a: "business",
//       b: "coding",
//       c: "ownership",
//       d: "development"
//     },
//     correctAnswer: "c"
//   },
//   {
//     question: "mutual respect is a core value?",
//     answers: {
//       a: "true",
//       b: "false"
//     }, 
//     correctAnswer: "a"
//   },
// {
//   question: "Javascript is a programming language",
//   answers: {
//     a: "true",
//     b: "false"
//   },
//   correctAnswer: "a"
// },
// {
//   question: "one of these is odd: Powertech, Introtech, Avitech, Garden data",
//   answers: {
//   a: "avitech",
//   b: "Garden data",
//   c: "Introtech",
//   d: "Powertech"
//   },
//   correctAnswer: "c"
// }
// ];
// buildQuiz();
// submitButton.addEventListener("click", showResults);

const progressBarFull = document.getElementById('progressBarFull');
const countDown = document.getElementById('countDown');
const timeLeft = document.getElementById('countdown');




(function() {

  const myQuestions = [
    {
question: "What is the full meaning of VGG?",
answers: {
a: "Venture Garden Group",
b: "Viable Ganja Ground ",
c: "none of the above",
d: "all of the above"
},
correctAnswer: "a"
},
{
question: "What year was VGG found?",
answers: {
a: "2018",
b: "1981",
c: "2010",
d: "2003"
},
correctAnswer: "c"
},
{
question: "How many SBUs' does VGG have?",
answers: {
a: "10",
b: "6",
c: "5",
d: "20"
},
correctAnswer: "c"
}, 
{
question: "Who is the CEO of VGG?",
answers: {
a: "Tesilimi",
b: "Seun",
c: "Theophilus",
d: "Bunmi"
},
correctAnswer: "d"
},
{
question: "What is imposible?",
answers: {
a: "everything",
b: "nothing",
c: "something",
d: "all of the above"
},
correctAnswer: "b"
},
{
question: "How many core values does VGG have?",
answers: {
a: "15",
b: "7",
c: "5",
d: "1"
},
correctAnswer: "c"
},
{
question: "One of these is a core value..",
answers: {
a: "business",
b: "coding",
c: "ownership",
d: "development"
},
correctAnswer: "c"
},
{
question: "mutual respect is a core value?",
answers: {
a: "true",
b: "false"
}, 
correctAnswer: "a"
},
{
question: "Javascript is a programming language",
answers: {
a: "true",
b: "false"
},
correctAnswer: "a"
},
{
question: "one of these is odd: Powertech, Introtech, Avitech, Garden data",
answers: {
a: "avitech",
b: "Garden data",
c: "Introtech",
d: "Powertech"
},
correctAnswer: "c"
    }
  ];






  


  function buildQuiz() {
    // we'll need a place to store the HTML output
    const output = [];

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // we'll want to store the list of answer choices
      const answers = [];

      // and for each available answer...
      for (letter in currentQuestion.answers) {
        // ...add an HTML radio button
        answers.push(
          `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
        );
      }

      // add this question and its answers to the output
      output.push(
        `<div class="slide">
           <div class="question"> ${currentQuestion.question} </div>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
      );
    });

    // finally combine our output list into one string of HTML and put it on the page
    quizContainer.innerHTML = output.join("");
  }

  function showResults() {
    // gather answer containers from our quiz
    const answerContainers = quizContainer.querySelectorAll(".answers");

    // keep track of user's answers
    let numCorrect = 0;

    // for each question...
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // find selected answer
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      // if answer is correct
      if (userAnswer === currentQuestion.correctAnswer) {
        // add to the number of correct answers
        numCorrect++;

        // color the answers green
        answerContainers[questionNumber].style.color = "lightgreen";
      } else {
        // if answer is wrong or blank
        // color the answers red
        answerContainers[questionNumber].style.color = "orange";
      }
    });

    // show number of correct answers out of total
    resultsContainer.innerHTML = `${numCorrect} out of ${myQuestions.length}`;
  }
//Slide
  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;
    
    if (currentSlide === 0) {
      previousButton.style.display = "none";
    } else {
      previousButton.style.display = "inline-block";
    }
    
    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
      submitButton.style.display = "inline-block";
    } else {
      nextButton.style.display = "inline-block";
      submitButton.style.display = "none";
    }
  }

  function showNextSlide() {
    questionCounter++;
    progressBarFull.style.width = `${(questionCounter / myQuestions.length) * 100}%`;
    showSlide(currentSlide + 1);
  }

  function showPreviousSlide() {
    questionCounter--;
    progressBarFull.style.width = `${(questionCounter / myQuestions.length) * 100}%`;
    showSlide(currentSlide - 1);
  }


  
  const maxQuestion = 10;
  let questionCounter = 0;
  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");

  function countdownTimer(duration, display) {  
  var timer = duration;
    var minutes;
    var seconds;
    var intervalId;
    intervalId = setInterval(function () {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);
  
  
      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;
      display.textContent = minutes + ":" + seconds;
  
  
      // if (--timer < 0) {
      //   timer = duration;
      // }
      if (--timer < 0 ){   
           clearInterval(intervalId);
           showResults();
      }
    }, 1000);
     
      
  
  
    // if (parseInt(minutes)==0 && parseInt(seconds)==0){
    //   // document.getElementById("countDown").innerHTML = "Time Remaining :"+min+" Minutes ," + sec+" Seconds";
    //    alert("Time Up");
    //  document.questionForm.minute.value=0;
    //  document.questionForm.second.value=0;
    //  document.questionForm.submit();
  
  
  
  }
  
  
  window.onload = function () {
    var threeMinutes = 60 * 3,
      display = document.querySelector('#countDown');
    countdownTimer(threeMinutes, display);
    };
  
  
  
  
  
  
  
  


  // display quiz right away
  buildQuiz();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  let currentSlide = 0;

  showSlide(0);

  // on submit, show results
  submitButton.addEventListener("click", showResults);
  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
})();

